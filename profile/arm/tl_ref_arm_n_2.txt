 
   Tea Version    1.200
       MPI Version
    OpenMP Version
   Task Count    128
 Thread Count:     1
 
 
   Tea Version    1.200
       MPI Version
    OpenMP Version
   Task Count    128
 Thread Count:     1
 
 Output file tea.out opened. All output will go there.
 Step       1 time   0.0000000 timestep   4.00E-03
Switching after  36 CG its, error   0.9708859E+00
Eigen min  0.848568E-02 Eigen max  0.209021E+01 Condition number    246.322688 Error  0.985335E+00
Conduction error  0.1356572E-15
Iteration count       43
PPCG Iteration count     280 (Total      323)
      Solve Time    1.8302879333    Its     44     Time Per It    0.0415974530
 Wall clock     1.831658840179443     
 Average time per cell    2.9306541442871095E-005
 Step time per cell       2.9306526184082031E-005
 Step       2 time   0.0040000 timestep   4.00E-03
Switching after   2 CG its, error   0.6211202E+02
Eigen min  0.848568E-02 Eigen max  0.209021E+01 Condition number    246.322688 Error  0.788112E+01
Conduction error  0.5828335E-16
Iteration count       11
PPCG Iteration count     336 (Total      347)
      Solve Time    1.9127881527    Its     12     Time Per It    0.1593990127
 Wall clock     3.763083934783936     
 Average time per cell    3.0104671478271486E-005
 Step time per cell       3.0876430511474608E-005
 Step       3 time   0.0080000 timestep   4.00E-03
Switching after   2 CG its, error   0.7674065E+01
Eigen min  0.848568E-02 Eigen max  0.209021E+01 Condition number    246.322688 Error  0.277021E+01
Conduction error  0.1164680E-15
Iteration count       11
PPCG Iteration count     336 (Total      347)
      Solve Time    1.8839480877    Its     12     Time Per It    0.1569956740
 Wall clock     5.648795843124390     
 Average time per cell    3.0126911163330078E-005
 Step time per cell       3.0148860931396483E-005
 Step       4 time   0.0120000 timestep   4.00E-03
Switching after   2 CG its, error   0.2646801E+01
Eigen min  0.848568E-02 Eigen max  0.209021E+01 Condition number    246.322688 Error  0.162690E+01
Conduction error  0.1185840E-15
Iteration count       11
PPCG Iteration count     336 (Total      347)
      Solve Time    1.9285628796    Its     12     Time Per It    0.1607135733
 Wall clock     7.579246044158936     
 Average time per cell    3.0316984176635742E-005
 Step time per cell       3.0862834930419924E-005
 Step       5 time   0.0160000 timestep   4.00E-03
Switching after   2 CG its, error   0.1525736E+01
Eigen min  0.848568E-02 Eigen max  0.209021E+01 Condition number    246.322688 Error  0.123521E+01
Conduction error  0.1093650E-15
Iteration count       11
PPCG Iteration count     336 (Total      347)
      Solve Time    1.8849840164    Its     12     Time Per It    0.1570820014
 Wall clock     9.465508937835693     
 Average time per cell    3.0289628601074219E-005
 Step time per cell       3.0165424346923828E-005
 Step       6 time   0.0200000 timestep   4.00E-03
Switching after   2 CG its, error   0.1088930E+01
Eigen min  0.848568E-02 Eigen max  0.209021E+01 Condition number    246.322688 Error  0.104352E+01
Conduction error  0.9956140E-16
Iteration count       11
PPCG Iteration count     336 (Total      347)
      Solve Time    1.8857088089    Its     12     Time Per It    0.1571424007
 Wall clock     11.35297989845276     
 Average time per cell    3.0274613062540690E-005
 Step time per cell       3.0177055358886718E-005
 Step       7 time   0.0240000 timestep   4.00E-03
Switching after   2 CG its, error   0.8486171E+00
Eigen min  0.848568E-02 Eigen max  0.209021E+01 Condition number    246.322688 Error  0.921204E+00
Conduction error  0.8764511E-16
Iteration count       11
PPCG Iteration count     336 (Total      347)
      Solve Time    1.9402780533    Its     12     Time Per It    0.1616898378
 Wall clock     13.29526305198669     
 Average time per cell    3.0389172690255301E-005
 Step time per cell       3.1050079345703126E-005
 Step       8 time   0.0280000 timestep   4.00E-03
Switching after   2 CG its, error   0.6888787E+00
Eigen min  0.848568E-02 Eigen max  0.209021E+01 Condition number    246.322688 Error  0.829987E+00
Conduction error  0.7711812E-16
Iteration count       11
PPCG Iteration count     336 (Total      347)
      Solve Time    1.8825891018    Its     12     Time Per It    0.1568824251
 Wall clock     15.17961692810059     
 Average time per cell    3.0359233856201174E-005
 Step time per cell       3.0127117156982423E-005
 Step       9 time   0.0320000 timestep   4.00E-03
Switching after   2 CG its, error   0.5717001E+00
Eigen min  0.848568E-02 Eigen max  0.209021E+01 Condition number    246.322688 Error  0.756109E+00
Conduction error  0.7112176E-16
Iteration count       11
PPCG Iteration count     336 (Total      347)
      Solve Time    1.9278697968    Its     12     Time Per It    0.1606558164
 Wall clock     17.10926795005798     
 Average time per cell    3.0416476355658638E-005
 Step time per cell       3.0851631164550779E-005
 Step      10 time   0.0360000 timestep   4.00E-03
Switching after   2 CG its, error   0.4812411E+00
Eigen min  0.848568E-02 Eigen max  0.209021E+01 Condition number    246.322688 Error  0.693715E+00
Conduction error  0.6864320E-16
Iteration count       11
PPCG Iteration count     336 (Total      347)
      Solve Time    1.8860220909    Its     12     Time Per It    0.1571685076
 Wall clock     18.99802994728088     
 Average time per cell    3.0396847915649415E-005
 Step time per cell       3.0195823669433594E-005
Test problem   3 is within   0.4864109E-09% of the expected solution
 This test is considered PASSED
 First step overhead  -9.8119020462036133E-002
 Wall clock     19.00134086608887     
 
Profiler Output                 Time            Percentage
Timestep              :          0.0181          0.0953
MPI Halo Exchange     :          3.6742         19.3367
Self Halo Update      :          0.0000          0.0000
Summary               :          0.0049          0.0260
Visit                 :          0.0000          0.0000
Tea Init              :          0.1377          0.7244
Dot Product           :          0.3122          1.6429
Tea Solve             :         14.8487         78.1453
Tea Reset             :          0.0114          0.0602
Set Field             :          0.0000          0.0000
Total                 :         19.0072        100.0309
The Rest              :         -0.0059         -0.0309
