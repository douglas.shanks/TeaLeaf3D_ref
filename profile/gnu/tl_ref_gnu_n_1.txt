PrgEnv/gnu-9.2/openmpi-4.0.2(13):ERROR:150: Module 'PrgEnv/gnu-9.2/openmpi-4.0.2' conflicts with the currently loaded module(s) 'PrgEnv/arm-20.0/openmpi-4.0.2'
PrgEnv/gnu-9.2/openmpi-4.0.2(13):ERROR:102: Tcl command execution failed: conflict	      PrgEnv/arm-20.0


   Tea Version    1.200
       MPI Version
    OpenMP Version
   Task Count     64
 Thread Count:     1


   Tea Version    1.200
       MPI Version
    OpenMP Version
   Task Count     64
 Thread Count:     1

 Output file tea.out opened. All output will go there.
 Step       1 time   0.0000000 timestep   4.00E-03
Switching after  36 CG its, error   0.9708859E+00
Eigen min  0.848568E-02 Eigen max  0.209021E+01 Condition number    246.322688 Error  0.985335E+00
Conduction error  0.1356572E-15
Iteration count       43
PPCG Iteration count     280 (Total      323)
      Solve Time    3.8984739780    Its     44     Time Per It    0.0886016813
 Wall clock    3.8997771739959717     
 Average time per cell    6.2396434783935543E-005
 Step time per cell       6.2396415710449222E-005
 Step       2 time   0.0040000 timestep   4.00E-03
Switching after   2 CG its, error   0.6211202E+02
Eigen min  0.848568E-02 Eigen max  0.209021E+01 Condition number    246.322688 Error  0.788112E+01
Conduction error  0.5828335E-16
Iteration count       11
PPCG Iteration count     336 (Total      347)
      Solve Time    4.1330239773    Its     12     Time Per It    0.3444186648
 Wall clock    8.0339331626892090     
 Average time per cell    6.4271465301513677E-005
 Step time per cell       6.6140079498291021E-005
 Step       3 time   0.0080000 timestep   4.00E-03
Switching after   2 CG its, error   0.7674065E+01
Eigen min  0.848568E-02 Eigen max  0.209021E+01 Condition number    246.322688 Error  0.277021E+01
Conduction error  0.1164680E-15
Iteration count       11
PPCG Iteration count     336 (Total      347)
      Solve Time    4.1304471493    Its     12     Time Per It    0.3442039291
 Wall clock    12.165608167648315     
 Average time per cell    6.4883243560791011E-005
 Step time per cell       6.6100288391113284E-005
 Step       4 time   0.0120000 timestep   4.00E-03
Switching after   2 CG its, error   0.2646801E+01
Eigen min  0.848568E-02 Eigen max  0.209021E+01 Condition number    246.322688 Error  0.162690E+01
Conduction error  0.1185840E-15
Iteration count       11
PPCG Iteration count     336 (Total      347)
      Solve Time    4.1318271160    Its     12     Time Per It    0.3443189263
 Wall clock    16.298542022705078     
 Average time per cell    6.5194168090820315E-005
 Step time per cell       6.6120445251464845E-005
 Step       5 time   0.0160000 timestep   4.00E-03
Switching after   2 CG its, error   0.1525736E+01
Eigen min  0.848568E-02 Eigen max  0.209021E+01 Condition number    246.322688 Error  0.123521E+01
Conduction error  0.1093650E-15
Iteration count       11
PPCG Iteration count     336 (Total      347)
      Solve Time    4.1318700314    Its     12     Time Per It    0.3443225026
 Wall clock    20.431519031524658     
 Average time per cell    6.5380860900878906E-005
 Step time per cell       6.6121170043945314E-005
 Step       6 time   0.0200000 timestep   4.00E-03
Switching after   2 CG its, error   0.1088930E+01
Eigen min  0.848568E-02 Eigen max  0.209021E+01 Condition number    246.322688 Error  0.104352E+01
Conduction error  0.9956140E-16
Iteration count       11
PPCG Iteration count     336 (Total      347)
      Solve Time    4.1061441898    Its     12     Time Per It    0.3421786825
 Wall clock    24.538765192031860     
 Average time per cell    6.5436707178751622E-005
 Step time per cell       6.5709407806396485E-005
 Step       7 time   0.0240000 timestep   4.00E-03
Switching after   2 CG its, error   0.8486171E+00
Eigen min  0.848568E-02 Eigen max  0.209021E+01 Condition number    246.322688 Error  0.921204E+00
Conduction error  0.8764511E-16
Iteration count       11
PPCG Iteration count     336 (Total      347)
      Solve Time    4.1406390667    Its     12     Time Per It    0.3450532556
 Wall clock    28.680512189865112     
 Average time per cell    6.5555456433977397E-005
 Step time per cell       6.6261474609375002E-005
 Step       8 time   0.0280000 timestep   4.00E-03
Switching after   2 CG its, error   0.6888787E+00
Eigen min  0.848568E-02 Eigen max  0.209021E+01 Condition number    246.322688 Error  0.829987E+00
Conduction error  0.7711812E-16
Iteration count       11
PPCG Iteration count     336 (Total      347)
      Solve Time    4.1339640617    Its     12     Time Per It    0.3444970051
 Wall clock    32.815613031387329     
 Average time per cell    6.5631226062774656E-005
 Step time per cell       6.6154819488525396E-005
 Step       9 time   0.0320000 timestep   4.00E-03
Switching after   2 CG its, error   0.5717001E+00
Eigen min  0.848568E-02 Eigen max  0.209021E+01 Condition number    246.322688 Error  0.756109E+00
Conduction error  0.7112176E-16
Iteration count       11
PPCG Iteration count     336 (Total      347)
      Solve Time    4.1384580135    Its     12     Time Per It    0.3448715011
 Wall clock    36.955225229263306     
 Average time per cell    6.5698178185356993E-005
 Step time per cell       6.6227008819580077E-005
 Step      10 time   0.0360000 timestep   4.00E-03
Switching after   2 CG its, error   0.4812411E+00
Eigen min  0.848568E-02 Eigen max  0.209021E+01 Condition number    246.322688 Error  0.693715E+00
Conduction error  0.6864320E-16
Iteration count       11
PPCG Iteration count     336 (Total      347)
      Solve Time    4.1305549145    Its     12     Time Per It    0.3442129095
 Wall clock    41.089388132095337     
 Average time per cell    6.5743021011352540E-005
 Step time per cell       6.6140129089355471E-005
Test problem   3 is within   0.5162093E-09% of the expected solution
 This test is considered PASSED
 First step overhead -0.23397898674011230     
 Wall clock    41.092456102371216     

Profiler Output                 Time            Percentage
Timestep              :          0.0061          0.0147
MPI Halo Exchange     :         10.4250         25.3695
Self Halo Update      :          0.0000          0.0000
Summary               :          0.0053          0.0130
Visit                 :          0.0000          0.0000
Tea Init              :          0.2769          0.6738
Dot Product           :          0.4109          1.0000
Tea Solve             :         29.9681         72.9285
Tea Reset             :          0.0210          0.0512
Set Field             :          0.0000          0.0000
Total                 :         41.1133        100.0507
The Rest              :         -0.0208         -0.0507
